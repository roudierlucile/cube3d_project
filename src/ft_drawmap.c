/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_drawmap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/07 10:41:42 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/07 13:27:48 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

void		set_pixel(t_map *map, int x, int y, int color)
{
	if (!(x < 0 || y < 0 || x >= map->widthwin || y >= map->heightwin))
		map->img.addr[y * map->img.line_length / 4 + x] = color;
}

void		ft_put_drawing(t_map *map, int x, int y)
{
	int		color;

	while (++y <= map->ray.drawend)
	{
		map->texture.ty = (int)map->texture.tpos &
			(map->imgtexture[map->texture.tdir].height - 1);
		map->texture.tpos += map->texture.s;
		if (y < map->heightwin && x < map->widthwin)
		{
			color = map->imgtexture[map->texture.tdir].addr[map->texture.ty \
				* map->imgtexture[map->texture.tdir].line_length / 4 \
				+ map->texture.tx];
			set_pixel(map, x, y, color);
		}
	}
}

void		ft_imgtexture_draw(t_map *map, int x, int y)
{
	y = map->ray.drawstart - 1;
	init_texture(map);
	map->texture.s = 1.0 * map->imgtexture[0].height / map->ray.lineheight;
	map->texture.tx = (int)(map->texture.wallx * (double)map->imgtexture
			[map->texture.tdir].width);
	if (map->ray.side == 0 && map->ray.raydirx > 0)
		map->texture.tx = map->imgtexture[map->texture.tdir].width -
			map->texture.tx - 1;
	if (map->ray.side == 1 && map->ray.raydiry < 0)
		map->texture.tx = map->imgtexture[map->texture.tdir].width -
			map->texture.tx - 1;
	map->texture.tpos = (map->ray.drawstart - map->heightwin / 2 +
			map->ray.lineheight / 2) * map->texture.s;
	ft_put_drawing(map, x, y);
}

void		ft_draw_col(t_map *map)
{
	int	x;
	int	y;
	int	color;

	x = -1;
	map->ray.drawend = map->heightwin - map->ray.drawstart;
	y = map->ray.drawend;
	while (++x < map->ray.drawstart)
	{
		color = (map->floor.red << 16) + (map->floor.green << 8) \
			+ map->floor.blue;
		map->img.addr[x * map->img.line_length / 4 + map->ray.x] = color;
	}
	if (x <= map->ray.drawend)
		ft_imgtexture_draw(map, map->ray.x, x);
	x = y;
	while (++x < map->heightwin)
	{
		color = (map->ceil.red << 16) + (map->ceil.green << 8) + map->ceil.blue;
		map->img.addr[x * map->img.line_length / 4 + map->ray.x] = color;
	}
}
