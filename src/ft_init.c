/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/04/14 18:01:19 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

void		ft_init_dir(t_map *map)
{
	if (map->start == 'F')
	{
		ft_putstr_fd("Error\n OH GOD YOUR MAP VERIFY YOUR MAP\n", 1);
		ft_free_datas(map, 1);
	}
	if (map->start == 'N')
		map->ray.dirx = -1;
	if (map->start == 'S')
		map->ray.dirx = 1;
	if (map->start == 'E')
		map->ray.diry = 1;
	if (map->start == 'W')
		map->ray.diry = -1;
	if (map->start == 'N')
		map->ray.planey = 0.66;
	if (map->start == 'S')
		map->ray.planey = -0.66;
	if (map->start == 'E')
		map->ray.planex = 0.66;
	if (map->start == 'W')
		map->ray.planex = -0.66;
}

void		init_pos(t_map *map)
{
	if (!(map->sprite.zbuffer = (double *)malloc(sizeof(double) \
		* map->widthwin)))
		exit(0);
	map->ray.posx = (double)map->px + 0.5;
	map->ray.posy = (double)map->py + 0.5;
	map->img.forward = 0;
	map->img.back = 0;
	map->img.left = 0;
	map->img.right = 0;
	map->img.rotate_right = 0;
	map->img.rotate_left = 0;
	map->ray.dirx = 0;
	map->ray.diry = 0;
	map->ray.planex = 0;
	map->ray.planey = 0;
	ft_init_dir(map);
}

void		init_map(t_map *map)
{
	map->mlx = mlx_init();
	ft_init_color(map);
	map->countmap = 0;
	map->nbrtext = 0;
	map->inside = NULL;
	map->img.img = NULL;
	map->imgtexture[0].img = NULL;
	map->imgtexture[1].img = NULL;
	map->imgtexture[2].img = NULL;
	map->imgtexture[3].img = NULL;
	map->imgtexture[4].img = NULL;
	map->win = NULL;
	map->valid = 1;
	map->height = 0;
	map->width = 0;
	map->widthscreen = 0;
	map->heightscreen = 0;
	map->save = 0;
	map->sprite.distance = NULL;
	map->sprite.order = NULL;
	map->sprite.zbuffer = NULL;
	map->vector = NULL;
}

void		init_texture(t_map *map)
{
	if (map->ray.side == 0 && map->ray.raydirx < 0)
		map->texture.tdir = 0;
	if (map->ray.side == 0 && map->ray.raydirx >= 0)
		map->texture.tdir = 1;
	if (map->ray.side == 1 && map->ray.raydiry < 0)
		map->texture.tdir = 2;
	if (map->ray.side == 1 && map->ray.raydiry >= 0)
		map->texture.tdir = 3;
	if (map->ray.side == 0)
		map->texture.wallx = map->ray.posy + map->ray.perpwalldist \
						* map->ray.raydiry;
	else
		map->texture.wallx = map->ray.posx + map->ray.perpwalldist \
						* map->ray.raydirx;
	map->texture.wallx -= floor((map->texture.wallx));
}

/*
** Initialisation de la structure de rat
** pos = à la position  du joueur en fonction de la direction
**  dir = la direction dans laquelle se trouve le joueur (SWEN)
**  plan = vecteur du plan (change en fonction de SWEN)
**	hit = si mur est touché
** perpwalldist = distance du joueur par rapport au mur
** camera = le point où se trouve la caméra (G= -1, M=0, D=1)
** raydir = direction du rayon
** map = coordonnée de la map où se trouve le le joueur
** movespeed = vitesse du joueur
** rotspeed = vitesse de la rotation
** et on calcul delta
*/

void		ft_init_ray(t_map *map)
{
	map->ray.hit = 0;
	map->ray.camerax = 2 * map->ray.x / (double)map->widthwin - 1;
	map->ray.raydirx = map->ray.dirx + map->ray.planex * map->ray.camerax;
	map->ray.raydiry = map->ray.diry + map->ray.planey * map->ray.camerax;
	map->ray.mapx = (int)map->ray.posx;
	map->ray.mapy = (int)map->ray.posy;
	map->ray.movespeed = 0.1;
	map->ray.rotspeed = 0.033 * 1.8;
	map->ray.deltadistx = fabs(1 / map->ray.raydirx);
	map->ray.deltadisty = fabs(1 / map->ray.raydiry);
}
