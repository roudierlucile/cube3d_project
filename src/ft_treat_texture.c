/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_treat_texture.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/04/14 18:05:06 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

int		ft_compare(char *set, char *line)
{
	int i;
	int	size;
	int	count;

	i = 0;
	size = ft_strlen(set);
	count = 0;
	while (line[i])
	{
		if (line[i] != ' ')
		{
			if (line[i] != set[count])
				return (1);
			count++;
		}
		if (count == size)
			return (0);
		i++;
	}
	return (1);
}

int		texture_are_ok(char *line, t_map *map)
{
	int	count;

	count = 0;
	if (ft_compare("NO./", line) == 0)
		if (!map->imgtexture[0].img)
			return (0);
	if (ft_compare("SO./", line) == 0)
		if (!map->imgtexture[1].img)
			return (0);
	if (ft_compare("WE./", line) == 0)
		if (!map->imgtexture[2].img)
			return (0);
	if (ft_compare("EA./", line) == 0)
		if (!map->imgtexture[3].img)
			return (0);
	if (ft_compare("S./", line) == 0)
		if (!map->imgtexture[4].img)
			return (0);
	return (1);
}

/*
** 	On attribue une adresse aux images texturés
**  Le choix d'un tableau car flemme de mettre tout ca dans une structure
**  et que je trouvais ca pas beau les structures
**  donc je t'explique vite fait!
** 	map->imgtexture[0] = texture NO
** 	map->imgtexture[1] = texture EA
** 	etc.. c'est simple et ca évite de me faire trop chier
*/

void	ft_get_texture_address(t_map *map, char *line)
{
	if (ft_compare("NO./", line) == 0)
		map->imgtexture[0].addr = (int *)mlx_get_data_addr(\
				map->imgtexture[0].img, &map->imgtexture[0].bits_per_pixel, \
				&map->imgtexture[0].line_length, &map->imgtexture[0].endian);
	else if (ft_compare("SO./", line) == 0)
		map->imgtexture[1].addr = (int*)mlx_get_data_addr(\
				map->imgtexture[1].img, &map->imgtexture[1].bits_per_pixel, \
				&map->imgtexture[1].line_length, &map->imgtexture[1].endian);
	else if (ft_compare("WE./", line) == 0)
		map->imgtexture[2].addr = (int*)mlx_get_data_addr(\
				map->imgtexture[2].img, &map->imgtexture[2].bits_per_pixel, \
				&map->imgtexture[2].line_length, &map->imgtexture[2].endian);
	else if (ft_compare("EA./", line) == 0)
		map->imgtexture[3].addr = (int*)mlx_get_data_addr(\
				map->imgtexture[3].img, &map->imgtexture[3].bits_per_pixel, \
				&map->imgtexture[3].line_length, &map->imgtexture[3].endian);
	else if (ft_compare("S./", line) == 0)
		map->imgtexture[4].addr = (int*)mlx_get_data_addr(\
				map->imgtexture[4].img, &map->imgtexture[4].bits_per_pixel, \
				&map->imgtexture[4].line_length, &map->imgtexture[4].endian);
}

/*
** Grossomodo cette fonction permet de convertir xpm file en image
** on regarde si il y a bien NO./ dans le fichier map.cub et hop on met l'image
*/

void	check_texture(t_map *map, char *line)
{
	int	i;

	i = 0;
	while (line[i])
	{
		if (line[i] == 'N' && line[i + 1] == 'O')
			map->nbrtext++;
		if (line[i] == 'S' && line[i + 1] == 'O')
			map->nbrtext++;
		if (line[i] == 'E' && line[i + 1] == 'A')
			map->nbrtext++;
		if (line[i] == 'W' && line[i + 1] == 'E')
			map->nbrtext++;
		if (ft_strchr("S", line[i]))
			map->nbrtext++;
		i++;
	}
}

void	get_imgtextures(t_map *map, char *line)
{
	check_texture(map, line);
	ft_put_texture(map, line);
	if (texture_are_ok(line, map) == 0)
	{
		ft_putstr_fd("Error\n Are u sure about your texture ?\n", 1);
		close_win(map);
	}
	ft_get_texture_address(map, line);
}
