/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_color.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/08 09:44:58 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./ft_cube3d.h"

char		*to_data(char *line, char c)
{
	if (c == '/')
	{
		while (*line != c && *line)
			line++;
	}
	else
	{
		while (ft_strchr("RCF", *line))
			line++;
	}
	return (line + 1);
}

/*
** un petit atoi pété juste pour convert les datas de map.cub
** la diff va avec count qui permet juste de
** savoir le nombre de char (genre "245" a 3 char)
*/

int			ft_atoi_size(char *str, int index)
{
	int	i;
	int	count;
	int	nb;

	count = 0;
	i = 0;
	str = to_data(str, 'R');
	while (i < ft_strlen(str) + 1 && str[i] && ((nb = 0) == 0))
	{
		if (!(ft_strchr(", 0123456789", str[i])))
			return (-1);
		while (str[i] == ' ' || str[i] == ',')
			i++;
		if (str[i] < '0' || str[i] > '9')
			return (-1);
		while (str[i] >= '0' && str[i] <= '9' && str[i])
		{
			nb = nb * 10 + (str[i] - 48);
			i++;
		}
		++count;
		if (count == index)
			return (nb);
	}
	return (-1);
}

void		ft_init_color(t_map *map)
{
	map->floor.red = 0;
	map->floor.green = 0;
	map->floor.blue = 0;
	map->ceil.red = 0;
	map->ceil.blue = 0;
	map->ceil.green = 0;
	map->widthwin = 500;
	map->heightwin = 500;
}

/*
** Get color permet de récupérer les couleurs données dans le map.cub
** - Meme principe pour C F, on check
** la fin de ligne et si Line contient
** la bonne lettre puis ont récupere les couleurs
** - Pour R Un peu pareil et on récupere la taille.
*/

int			get_color(char *line, t_map *map)
{
	int	ok;

	ok = 1;
	if (ft_strchr(line, 'F') && check_line_end(line, 3))
	{
		map->floor.red = ft_atoi_size(line, 1);
		map->floor.green = ft_atoi_size(line, 2);
		map->floor.blue = ft_atoi_size(line, 3);
	}
	else if (ft_strchr(line, 'C') && check_line_end(line, 3))
	{
		map->ceil.red = ft_atoi_size(line, 1);
		map->ceil.green = ft_atoi_size(line, 2);
		map->ceil.blue = ft_atoi_size(line, 3);
	}
	else if (ft_strchr(line, 'R') && check_line_end(line, 2))
	{
		map->widthwin = ft_atoi_size(line, 1);
		map->heightwin = ft_atoi_size(line, 2);
	}
	if (map->floor.red < 0 || map->floor.green < 0 || map->floor.blue < 0
		|| map->ceil.red < 0 || map->ceil.green < 0 || map->ceil.blue < 0)
		ok = 0;
	return (ok);
}
