/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/04/15 11:08:43 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./ft_cube3d.h"

int		check_extension(char *line)
{
	int	i;
	int	x;

	i = ft_strlen(line);
	x = 0;
	if (i > 5)
	{
		if (line[i - 4] != '.' || line[i - 3] != 'c' || line[i - 2] != 'u' \
			|| line[i - 1] != 'b')
		{
			ft_putstr_fd("Error\n Wrong file type\n", 1);
			return (-1);
		}
	}
	return (1);
}

int		ft_strlen_map(char *line, t_map *map, int player)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	while (line[i])
	{
		if (((line[i] >= '0' && line[i] <= '4') \
		|| line[i] == 'N' || line[i] == \
		'S' || line[i] == 'W' || line[i] == 'E') && line[i])
			count++;
		if (ft_strchr("NSWE", line[i]) && (player == 1))
			map->player++;
		i++;
	}
	return (count);
}

/*
** 	Cette fonction va permettre de parser un peu le map.cub
** 	donc on va vérifier si la ligne du fichier a des espaces
** 	et on avance parce que on s'en fiche
** 	on va vérifier si on trouve les textures de NSWE) et on va les traiter
**  On va vérifier le floor, ceil etc, et on traite
**	On va vérifier si la map est ok et si y a pas de mauvais char
*/

int		parse_data_maps(t_map *map, char *line)
{
	int	i;

	i = 0;
	while (*line == ' ' && *line)
		line++;
	if (ft_strchr("NSWE", line[0]))
		get_imgtextures(map, line);
	else if (ft_strchr("FCR", line[0]))
	{
		map->countmap++;
		if (!(get_color(line, map)))
			return (0);
	}
	if (line[0] == '1' && !(check_map(line, map)))
		return (0);
	if (line[0] == '0')
		return (0);
	if (!(ft_strchr("102SNWERCF\n\0", line[0])))
		return (0);
	else
		return (1);
	return (0);
}

/*
** Parse arg va check le fichier si il est bon ou pas
** - on vérifier l'extension, si c'est ok on l'ouvre
** parce qu'on veut pas le modifier on veut juste le lire
** - on utilise get_next line pour voir les lignes et traiter chaque ligne
** - puis on parse la derniere ligne;
*/

int		check_first_end(t_map *map)
{
	int	j;

	j = -1;
	while (++j < map->width)
	{
		if (map->inside[0][j] != '1')
			return (0);
		if (map->inside[map->height - 1][j] != '1')
			return (0);
	}
	return (1);
}

int		parse_args(char *argv[], t_map *map)
{
	int		fd;
	char	*line;

	map->player = 0;
	if (check_extension(argv[1]) == 1)
		fd = open(argv[1], O_RDONLY);
	else
		return (0);
	while (get_next_line(fd, &line) > 0)
	{
		if (!parse_data_maps(map, line))
		{
			free(line);
			return (0);
		}
		free(line);
	}
	if (!check_first_end(map))
		return (0);
	close(fd);
	return (1);
}
