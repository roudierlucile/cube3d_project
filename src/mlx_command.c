/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_command.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/07 10:55:21 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

void		ft_free_datas(t_map *map, int check)
{
	int	i;

	i = -1;
	if (map->inside)
	{
		while (++i < map->height)
			free(map->inside[i]);
		free(map->inside);
	}
	if (map->vector)
		free(map->vector);
	if (map->sprite.zbuffer)
		free(map->sprite.zbuffer);
	if (map->sprite.distance)
		free(map->sprite.distance);
	if (map->sprite.order)
		free(map->sprite.order);
	if (check == 1)
		exit(0);
}

int			close_win(t_map *map)
{
	ft_free_datas(map, 0);
	if (map->img.img)
		mlx_destroy_image(map->mlx, map->img.img);
	if (map->imgtexture[0].img)
		mlx_destroy_image(map->mlx, map->imgtexture[0].img);
	if (map->imgtexture[1].img)
		mlx_destroy_image(map->mlx, map->imgtexture[1].img);
	if (map->imgtexture[2].img)
		mlx_destroy_image(map->mlx, map->imgtexture[2].img);
	if (map->imgtexture[3].img)
		mlx_destroy_image(map->mlx, map->imgtexture[3].img);
	if (map->imgtexture[4].img)
		mlx_destroy_image(map->mlx, map->imgtexture[4].img);
	if (map->win)
		mlx_destroy_window(map->mlx, map->win);
	if (map)
		free(map);
	exit(0);
}

int			move_command(int keycode, t_map *map)
{
	if (keycode == 65307)
		close_win(map);
	if (keycode == 119)
		map->img.forward = 1;
	if (keycode == 115)
		map->img.back = 1;
	if (keycode == 100)
		map->img.left = 1;
	if (keycode == 113)
		map->img.right = 1;
	if (keycode == 65361)
		map->img.rotate_right = 1;
	if (keycode == 65363)
		map->img.rotate_left = 1;
	return (1);
}

int			move_command_release(int keycode, t_map *map)
{
	if (keycode == 119)
		map->img.forward = 0;
	if (keycode == 115)
		map->img.back = 0;
	if (keycode == 100)
		map->img.left = 0;
	if (keycode == 113)
		map->img.right = 0;
	if (keycode == 65361)
		map->img.rotate_right = 0;
	if (keycode == 65363)
		map->img.rotate_left = 0;
	return (1);
}
