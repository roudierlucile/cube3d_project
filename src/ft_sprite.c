/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sprite.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/07 10:29:55 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/07 11:10:24 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

void		ft_order_sprite(t_map *map)
{
	int		i;
	int		j;
	double	tmp;

	ft_sort_sprite(map);
	i = -1;
	while (++i < map->sprite.nsprite)
	{
		j = -1;
		while (++j < map->sprite.nsprite - 1)
		{
			if (map->sprite.distance[j] < map->sprite.distance[j + 1])
			{
				tmp = map->sprite.distance[j];
				map->sprite.distance[j] = map->sprite.distance[j + 1];
				map->sprite.distance[j + 1] = tmp;
				tmp = map->sprite.order[j];
				map->sprite.order[j] = map->sprite.order[j + 1];
				map->sprite.order[j + 1] = (int)tmp;
			}
		}
	}
}

void		ft_calcul_sprite(t_map *map)
{
	map->sprite.spriteheight = abs((int)(map->heightwin / \
				(map->sprite.transformy)));
	map->sprite.drawstarty = -map->sprite.spriteheight / 2 + map->heightwin / 2;
	if (map->sprite.drawstarty < 0)
		map->sprite.drawstarty = 0;
	map->sprite.drawendy = map->sprite.spriteheight / 2 + map->heightwin / 2;
	if (map->sprite.drawendy >= map->heightwin)
		map->sprite.drawendy = map->heightwin;
	map->sprite.spritewidth = abs((int)(map->heightwin / \
				(map->sprite.transformy)));
	map->sprite.drawstartx = -map->sprite.spritewidth / 2 \
				+ map->sprite.spritescreenx;
	if (map->sprite.drawstartx < 0)
		map->sprite.drawstartx = 0;
	map->sprite.drawendx = map->sprite.spritewidth / 2 \
				+ map->sprite.spritescreenx;
	if (map->sprite.drawendx >= map->widthwin)
		map->sprite.drawendx = map->widthwin;
}

/*
**  fonction fait projetion et les dessines
*/

void		ft_dist_sprite(t_map *map, int i)
{
	map->sprite.sprite_x = map->vector[map->sprite.order[i]].x - map->ray.posx;
	map->sprite.sprite_y = map->vector[map->sprite.order[i]].y - map->ray.posy;
	map->sprite.invdet = 1.0 / (map->ray.planex * map->ray.diry -
			map->ray.dirx * map->ray.planey);
	map->sprite.transformx = map->sprite.invdet * (map->ray.diry *
			map->sprite.sprite_x - map->ray.dirx * map->sprite.sprite_y);
	map->sprite.transformy = map->sprite.invdet * (-map->ray.planey *
			map->sprite.sprite_x + map->ray.planex * map->sprite.sprite_y);
	map->sprite.spritescreenx = (int)((map->widthwin / 2) * \
			(1 + map->sprite.transformx / map->sprite.transformy));
	ft_calcul_sprite(map);
}

void		vertical_stripe(t_map *map, int texx, int stripe)
{
	int		d;
	int		texy;
	int		y;

	y = map->sprite.drawstarty - 1;
	while (++y < map->sprite.drawendy)
	{
		d = (y) * 256 - map->heightwin * 128 + map->sprite.spriteheight * 128;
		texy = ((d * map->imgtexture[4].height) / \
				map->sprite.spriteheight) / 256;
		if (map->imgtexture[4].addr[texy *\
				map->imgtexture[4].line_length / 4 + texx] != 0x000000)
			set_pixel(map, stripe, y,\
					map->imgtexture[4].addr[texy *\
					map->imgtexture[4].line_length / 4 + texx]);
	}
}

void		ft_start_sprite(t_map *map)
{
	int		i;
	int		stripe;
	int		texx;

	i = -1;
	ft_order_sprite(map);
	while (++i < map->sprite.nsprite)
	{
		ft_dist_sprite(map, i);
		stripe = map->sprite.drawstartx - 1;
		while (++stripe < map->sprite.drawendx)
		{
			texx = (int)(256 * (stripe - (-map->sprite.spritewidth / 2 +
							map->sprite.spritescreenx)) *\
							map->imgtexture[4].width
							/ map->sprite.spritewidth) / 256;
			if (map->sprite.transformy > 0 && stripe >= 0 \
					&& stripe < map->widthwin
					&& map->sprite.transformy < map->sprite.zbuffer[stripe])
				vertical_stripe(map, texx, stripe);
		}
	}
}
