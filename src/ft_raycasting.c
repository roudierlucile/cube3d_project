/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_raycasting.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/02/28 14:48:41 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./ft_cube3d.h"

/*
** On va calculer la h'auteur de la ligne verticale à tracer
** c'est à dire, les positions de début et de fin de l'
** endroit où on doit vraiment dessiner
** en gros c'est la vision du joueur. Le centrer
** du mur doit être au centre de l'écran alors.
*/

void		ft_height_of_line(t_map *map)
{
	map->ray.lineheight = (int)(map->heightwin / map->ray.perpwalldist);
	map->ray.drawstart = -map->ray.lineheight / 2 + map->heightwin / 2;
	if (map->ray.drawstart < 0)
		map->ray.drawstart = 0;
	map->ray.drawend = map->ray.lineheight / 2 + map->heightwin / 2;
	if (map->ray.drawend >= map->heightwin || map->ray.drawend < 0)
		map->ray.drawend = map->heightwin - 1;
}

/*
** on va calculer la distance projetée sur la dir de la caméra (le côté fisheye)
** si le côté touché est x  on calcul la distance entre mur
** et joueur en fonction de x
** sinon c'est en fonction de y
** En fait on va calculer en fonction de la caméra
** parce que comme ça ca éviter de donner un aspect
** arrondis aux murs droits.
*/

void		ft_dist_cam_dir(t_map *map)
{
	if (map->ray.side == 0)
		map->ray.perpwalldist = ((double)map->ray.mapx - \
				map->ray.posx + (1 - (double)map->ray.stepx)
				/ 2) / map->ray.raydirx;
	else
		map->ray.perpwalldist = ((double)map->ray.mapy - map->ray.posy + \
				(1 - (double)map->ray.stepy) / 2) /
					map->ray.raydiry;
	map->ray.lineheight = (int)((double)map->heightwin /
			(double)map->ray.perpwalldist);
	map->ray.drawstart = -map->ray.lineheight / 2 + map->heightwin / 2;
	if (map->ray.drawstart < 0)
		map->ray.drawstart = 0;
	map->ray.drawend = map->ray.lineheight / 2 + map->heightwin / 2;
	if (map->ray.drawend >= map->heightwin || map->ray.drawend < 0)
		map->ray.drawend = map->heightwin - 1;
}

/*
**  DDA = Digital data analyse
** C'est une boucle qui incrémente le rayon de 1 case
** jusqu'à ce qu'un mur soit touché
**  En fait il saute d'un carrée à la fois ou dans la dir x ou y
**  et lorsqu'un mur a était touché on arrête la bouchle et hit prend 1;
*/

void		ft_perform_dda(t_map *map)
{
	while (map->ray.hit == 0)
	{
		if (map->ray.sidedistx < map->ray.sidedisty)
		{
			map->ray.sidedistx += map->ray.deltadistx;
			map->ray.mapx += map->ray.stepx;
			map->ray.side = 0;
		}
		else
		{
			map->ray.sidedisty += map->ray.deltadisty;
			map->ray.mapy += map->ray.stepy;
			map->ray.side = 1;
		}
		if (map->inside[map->ray.mapx][map->ray.mapy] == '1')
			map->ray.hit = 1;
	}
}

/*
**  Va permettre d'initialiser sidedist
** en fait sidedist va récupérer la distance qu'un rayon parcours
** jusqu'au premier côté qu'il touche
** et ca en fait on va le faire pour chaque rayon
**  step en fait va permettre de sauter une case en fonction
** si c'est horizontal ou vertical
*/

void		ft_init_sidedist(t_map *map)
{
	if (map->ray.raydirx < 0)
	{
		map->ray.stepx = -1;
		map->ray.sidedistx = (map->ray.posx - map->ray.mapx) \
				* map->ray.deltadistx;
	}
	else
	{
		map->ray.stepx = 1;
		map->ray.sidedistx = (map->ray.mapx + 1.0 - map->ray.posx) \
				* map->ray.deltadistx;
	}
	if (map->ray.raydiry < 0)
	{
		map->ray.stepy = -1;
		map->ray.sidedisty = (map->ray.posy - map->ray.mapy) \
				* map->ray.deltadisty;
	}
	else
	{
		map->ray.stepy = 1;
		map->ray.sidedisty = (map->ray.mapy + 1.0 - map->ray.posy) \
				* map->ray.deltadisty;
	}
}
