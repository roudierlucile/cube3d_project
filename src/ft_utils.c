/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/07 10:38:16 by lroudier          #+#    #+#             */
/*   Updated: 2021/04/14 18:02:45 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

void		ft_pos_sprite(t_map *map, int i, int j, int v)
{
	i = i - 1;
	while (++i < map->height)
	{
		j = -1;
		while (++j < map->width)
		{
			if (map->inside[i][j] == '2')
			{
				map->vector[v].x = (double)i + 0.5;
				map->vector[v].y = (double)j + 0.5;
				v++;
			}
		}
	}
}

void		ft_malloc_sprite(t_map *map)
{
	if (!(map->vector = (t_vector *)malloc(sizeof(t_vector) \
					* map->sprite.nsprite)))
	{
		close_win(map);
		ft_putstr_fd("Error\n Error with malloc vector \n", 1);
	}
	if (!(map->sprite.order = (int *)malloc(sizeof(int) \
					* map->sprite.nsprite)))
	{
		close_win(map);
		ft_putstr_fd("Error\n Problem with the order sprite\n", 1);
	}
	if (!(map->sprite.distance = (double *)malloc(sizeof(double) \
					* map->sprite.nsprite)))
	{
		close_win(map);
		ft_putstr_fd("Error\n Problem with the distance sprite", 1);
	}
}

void		ft_nbr_sprite_pos(t_map *map)
{
	int		i;
	int		j;

	i = -1;
	map->sprite.nsprite = 0;
	while (++i < map->height)
	{
		j = -1;
		while (++j < map->width)
		{
			if (map->inside[i][j] == '2')
				map->sprite.nsprite += 1;
		}
	}
	ft_malloc_sprite(map);
	ft_pos_sprite(map, 0, 0, 0);
}

void		ft_sort_sprite(t_map *map)
{
	int i;

	i = -1;
	while (++i < map->sprite.nsprite)
	{
		map->sprite.order[i] = i;
		map->sprite.distance[i] = ((map->ray.posx - map->vector[i].x) *
				(map->ray.posx - map->vector[i].x) + (map->ray.posy -
					map->vector[i].y) * (map->ray.posy - map->vector[i].y));
	}
}

void		ft_check_pos_ray(t_map *map, int i, int j)
{
	if (map->inside[i][j] == 'N' || map->inside[i][j] == 'S' \
			|| map->inside[i][j] == 'W' || map->inside[i][j] == 'E')
	{
		map->start = map->inside[i][j];
		map->px = i;
		map->py = j;
	}
}
