/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_treat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/07 11:24:31 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./ft_cube3d.h"

/*
** Copy map permet de copier les datas de map.cub dans map->inside
** 	c'est assez symple, on malloc **temp puis on le remplie pour l'enlever
*/

char		**copy_map(t_map *map, int lines)
{
	int		i;
	int		j;
	char	**temp;

	i = -1;
	map->start = 'F';
	if (!(temp = malloc(sizeof(char*) * (lines + 1))))
		return (0);
	while (++i < lines + 1)
		if (!(temp[i] = malloc(sizeof(char) * map->width)))
			return (0);
	i = -1;
	while (++i < lines)
	{
		j = -1;
		while (++j < map->width)
		{
			ft_check_pos_ray(map, i, j);
			temp[i][j] = map->inside[i][j];
		}
	}
	return (temp);
}

/*
** C'est la qu'on va rajouter les lignes
** on va parcourir tant qu'il y a width puis on remplis
** on free
** et on récupère le height (parce que je te jure que c'était galere)
** et on met temp dans inside et boom
*/

int			add_line(t_map *map, char *line)
{
	int		i;
	char	**temp;

	i = 0;
	if (!(temp = copy_map(map, map->height)))
		return (0);
	while (i < map->width)
	{
		temp[map->height][i] = line[i];
		i++;
	}
	i = -1;
	if (map->height > 0)
	{
		while (++i < map->height)
			free(map->inside[i]);
		free(map->inside);
	}
	map->height += 1;
	map->inside = temp;
	return (1);
}

/*
** On enleve les espacesdes lignes
** et ce qu'on va récup c'est la ligne sans les espaces
** genre " 1 0 0 0 0 1" devient "100001"
*/

char		*out_space(char *line, t_map *map)
{
	int		size;
	char	*temp;
	int		i;
	int		j;

	i = 0;
	j = 0;
	size = ft_strlen_map(line, map, 0);
	if (!(temp = malloc(sizeof(char) * (size + 1))))
		return (0);
	while (line[i])
	{
		if ((line[i] >= '0' && line[i] <= '2') || ft_strchr("SNWE", line[i]))
			temp[j++] = line[i];
		i++;
	}
	temp[j] = '\0';
	return (temp);
}

/*
** On check si tout est ok dans la map
** on recupe la taille
** on enleve les espace de la ligne
** si c'est pas bon on return 0
** après on check la suite si l'emplacement est bon pour les 1 et le reste
** et on add la line
*/

int			check_map(char *line, t_map *map)
{
	int		nb;
	int		i;
	char	*temp;

	i = -1;
	nb = 0;
	if (nb == 0)
		map->width = ft_strlen_map(line, map, 1);
	if (!(temp = out_space(line, map)))
		return (0);
	i = -1;
	while (temp[++i] && map->valid == 1)
	{
		if (temp[0] != '1' || temp[map->width - 1] != '1' \
			|| (temp[i] != '1' && nb == 0) || !(ft_strchr("012SNWE", temp[i])))
			map->valid = 0;
		if (ft_strchr("SNWE", temp[i]))
			temp[i] = '0';
	}
	if (!add_line(map, temp))
		return (0);
	free(temp);
	return ((nb = nb + 1));
}

int			check_line_end(char *line, int nb)
{
	while (*line == ' ')
		line++;
	line++;
	while (nb > 0 && *line)
	{
		if (*line == ' ' || *line == ',')
			line++;
		else if (*line <= '9' && *line >= '0' && nb--)
		{
			while (*line <= '9' && *line >= '0' && *line)
				line++;
		}
		else
			break ;
	}
	while (*line)
	{
		if (*line != ' ')
			return (0);
		line++;
	}
	return (1);
}
