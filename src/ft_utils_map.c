/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/07 10:38:16 by lroudier          #+#    #+#             */
/*   Updated: 2021/04/15 10:54:54 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

void	ft_put_texture(t_map *map, char *line)
{
	if (ft_compare("NO./", line) == 0)
		map->imgtexture[0].img = mlx_xpm_file_to_image(map->mlx, \
				to_data(line, '/'), &map->imgtexture[0].width, \
				&map->imgtexture[0].height);
	else if (ft_compare("SO./", line) == 0)
		map->imgtexture[1].img = mlx_xpm_file_to_image(map->mlx, \
				to_data(line, '/'), &map->imgtexture[1].width, \
				&map->imgtexture[1].height);
	else if (ft_compare("WE./", line) == 0)
		map->imgtexture[2].img = mlx_xpm_file_to_image(map->mlx, \
				to_data(line, '/'), &map->imgtexture[2].width, \
				&map->imgtexture[3].height);
	else if (ft_compare("EA./", line) == 0)
		map->imgtexture[3].img = mlx_xpm_file_to_image(map->mlx, \
				to_data(line, '/'), &map->imgtexture[3].width, \
				&map->imgtexture[4].height);
	else if (ft_compare("S./", line) == 0)
		map->imgtexture[4].img = mlx_xpm_file_to_image(map->mlx, \
				to_data(line, '/'), &map->imgtexture[4].width, \
				&map->imgtexture[4].height);
}
