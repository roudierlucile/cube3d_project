/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cube3D.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 10:13:06 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/08 10:23:42 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CUBE3D_H
# define FT_CUBE3D_H

# include "../libft/libft.h"
# include "../minilibx/mlx.h"
# include <fcntl.h>
# include <math.h>

typedef struct	s_floor
{
	int		red;
	int		green;
	int		blue;
}				t_floor;

typedef struct	s_ceil
{

	int		red;
	int		green;
	int		blue;
}				t_ceil;

typedef struct	s_texture
{
	int		tdir;
	double	wallx;
	int		tx;
	int		ty;
	double	s;
	double	tpos;
}				t_texture;

typedef struct	s_ray
{
	double	posx;
	double	posy;
	double	dirx;
	double	diry;
	double	planex;
	double	planey;
	double	raydirx;
	double	raydiry;
	double	camerax;
	int		mapx;
	int		mapy;
	double	sidedistx;
	double	sidedisty;
	double	deltadistx;
	double	deltadisty;
	int		stepx;
	int		stepy;
	int		hit;
	int		side;
	double	perpwalldist;
	int		lineheight;
	int		drawstart;
	int		drawend;
	double	movespeed;
	double	rotspeed;
	int		x;
	int		texture;
}				t_ray;

typedef struct	s_data {
	void	*img;
	int		*addr;
	int		bits_per_pixel;
	int		line_length;
	int		endian;
	int		forward;
	int		back;
	int		left;
	int		width;
	int		height;
	int		right;
	int		rotate_left;
	int		rotate_right;
}				t_data;

typedef struct	s_vector
{
	double	x;
	double	y;
}				t_vector;

typedef struct	s_sprite
{
	int		*order;
	int		nsprite;
	double	*distance;
	double	sprite_x;
	double	sprite_y;
	double	invdet;
	double	transformx;
	double	transformy;
	int		spritescreenx;
	int		spriteheight;
	int		drawstartx;
	int		drawstarty;
	int		drawendy;
	int		drawendx;
	int		spritewidth;
	double	*zbuffer;

}				t_sprite;

typedef struct	s_map
{
	void		*mlx;
	void		*win;
	int			player;
	char		start;
	int			width;
	int			height;
	int			widthwin;
	int			heightwin;
	int			widthscreen;
	int			heightscreen;
	int			valid;
	int			pos_y;
	int			pos_x;
	int			dirx;
	int			px;
	int			py;
	int			diry;
	int			nbrtext;
	int			countmap;
	char		**inside;
	t_ceil		ceil;
	t_floor		floor;
	t_data		imgtexture[5];
	t_data		img;
	t_ray		ray;
	t_vector	*vector;
	t_sprite	sprite;
	t_texture	texture;
	int			save;
}				t_map;

void			ft_save_bmp(t_map *map);
int				check_bmp(char *arg);
void			init_pos(t_map *map);
void			init_map(t_map *map);
void			init_sprite(t_map *map);
char			*to_data(char *line, char c);
void			init_map_draw(t_map *map);
void			ft_put_texture(t_map *map, char *line);
int				ft_compare(char *set, char *line);
int				close_win(t_map *map);
int				parse_args(char *argv[], t_map *map);
int				move_command(int keycode, t_map *map);
int				move_command_release(int keycode, t_map *map);
int				ft_strlen_map(char *line, t_map *map, int player);
void			ft_move_y(t_map *map);
void			ft_move_x(t_map *map);
void			ft_move_rotate_x(t_map *map);
void			ft_init_sidedist(t_map *map);
void			ft_init_ray(t_map *map);
void			ft_move_y(t_map *map);
void			ft_move_x(t_map *map);
void			ft_move_rotate_x(t_map *map);
void			get_imgtextures(t_map *map, char *line);
void			init_texture(t_map *map);
void			ft_init_dir(t_map *map);
void			ft_dist_cam_dir(t_map *map);
void			ft_stepsidedist(t_map *map);
void			ft_perform_dda(t_map *map);
void			ft_dist_cam_dir(t_map *map);
void			ft_nbr_sprite_pos(t_map *map);
int				check_line_end(char *line, int nb);
void			ft_start_sprite(t_map *map);
int				get_color(char *line, t_map *map);
int				check_map(char *line, t_map *map);
void			ft_free_datas(t_map *map, int check);
void			set_pixel(t_map *map, int x, int y, int color);
void			ft_draw_col(t_map *map);
void			ft_nbr_sprite_pos(t_map *map);
void			ft_sort_sprite(t_map *map);
void			ft_check_pos_ray(t_map *map, int i, int j);
void			ft_init_color(t_map *map);

#endif
