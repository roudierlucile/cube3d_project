/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/03/07 10:27:49 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

/*
**  Création du fichier BITMAP
**  voir comment ca fonctionne:
**  https://www.commentcamarche.net/contents/1200-bmp-format-bmp
**  on commence parce le header
**      BM car c'est un BITMAP WINDOWS
**      14 + 40 +4 * size = taille total du fichier sur 4 octets
**      on met à 0
**      La largeur de l'image sur 4 octets
**      La hauteur de l'image sur 4 octet
**      le nombre de plans (par def 1 sur 2 octs)
**      profondeur de couleur sur 2 octets
**      methode de compression sur 4 octets
**      taille totale de l'image en octet
**      nbr de pixels par metre horizontalement (4octets)
**      resolution vertical
**      nbr de couleur de la palettes
**      nbr de couleur importante
*/

void		ft_headheur(t_map *map, int fd)
{
	int	size;
	int	pixel;
	int	header;
	int	plane;
	int	img;

	pixel = 54;
	size = pixel + map->widthwin * map->heightwin * 4;
	write(fd, "BM", 2);
	write(fd, &size, 4);
	write(fd, "\0\0\0\0", 4);
	write(fd, &pixel, 4);
	header = 40;
	plane = 1;
	img = map->widthwin * map->heightwin;
	write(fd, &header, 4);
	write(fd, &map->widthwin, 4);
	write(fd, &map->heightwin, 4);
	write(fd, &plane, 2);
	write(fd, &map->img.bits_per_pixel, 2);
	write(fd, "\0\0\0\0", 4);
	write(fd, &img, 4);
	write(fd, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 16);
}

/*
**  Va permettre de créer un fichier BMP
**  Il va créer un fichier si il n'existe
** pas ou l'ouvrir (RDWR = ouvrir & Ecrre)
**  on va parcourir toute l'image (height & weight) et on va écrire dedans
**     Parce que le codage de l'image se fait ligne par
** ligne et commençant en bas à gauche
*/

void		ft_save_bmp(t_map *map)
{
	int		fd;
	int		x;
	int		y;

	y = map->heightwin;
	if ((fd = open("./save.bmp", O_CREAT | O_RDWR | O_TRUNC, 0777)) == -1)
		ft_putstr_fd("Error\n Problem with the creation of BMP", 1);
	ft_headheur(map, fd);
	while (y >= 0)
	{
		x = -1;
		while (++x < map->widthwin)
			write(fd, &map->img.addr[y * map->img.line_length / 4 + x], 4);
		y--;
	}
	close(fd);
	ft_putstr_fd("Success\n The screenshot is okay. \n", 1);
	ft_free_datas(map, 1);
}

int			check_bmp(char *arg)
{
	int		i;

	i = ft_strlen(arg);
	if (i < 7 && arg[i - 1] == 'e' && arg[i - 2] == 'v' && arg[i - 3] == 'a'
			&& arg[i - 4] == 's' && arg[i - 5] == '-' && arg[i - 6] == '-')
		return (1);
	return (0);
}
