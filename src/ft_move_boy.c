/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_boy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2020/12/15 10:19:48 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cube3d.h"

int			ft_collision(int x, int y, t_map *map)
{
	if (map->inside[x][y] == '1' || map->inside[x][y] == '2')
		return (0);
	return (1);
}

/*
** ft_move_y et move_x ont le meme principe.
** On detecte via au commandes de touches (fleches directionnelles)
** forward = 1 c'est en avant forward = -1 c'est en arriere
** - Si la case est = 0 la position de x
** est += à la direction * la vitesse de move
** - pareil pour y
** - et on fait la meme chose pour en arriere
** sauf que on fait pas + mais - lors des calculs.
*/

void		ft_move_y(t_map *map)
{
	if (map->img.forward == 1)
	{
		if (ft_collision((int)(map->ray.posx - \
			(map->ray.dirx * (map->ray.movespeed))), \
			(int)(map->ray.posy - (map->ray.diry * (map->ray.movespeed))), map))
		{
			map->ray.posx -= map->ray.dirx * (map->ray.movespeed / 2.5);
			map->ray.posy -= map->ray.diry * (map->ray.movespeed / 2.5);
		}
	}
	if (map->img.back == 1)
	{
		if (ft_collision((int)(map->ray.posx + \
			(map->ray.dirx * (map->ray.movespeed))), \
			(int)(map->ray.posy + (map->ray.diry * (map->ray.movespeed))), map))
		{
			map->ray.posx += map->ray.dirx * (map->ray.movespeed / 2.5);
			map->ray.posy += map->ray.diry * (map->ray.movespeed / 2.5);
		}
	}
}

void		ft_move_x(t_map *map)
{
	if (map->img.right == 1)
	{
		if (ft_collision((int)(map->ray.posx - \
			(map->ray.planex * (map->ray.movespeed))), \
			(int)(map->ray.posy - (map->ray.planey * \
			(map->ray.movespeed))), map))
		{
			map->ray.posx -= map->ray.planex * 0.1;
			map->ray.posy -= map->ray.planey * 0.1;
		}
	}
	if (map->img.left == 1)
	{
		if (ft_collision((int)(map->ray.posx + \
			(map->ray.planex * (map->ray.movespeed))), \
			(int)(map->ray.posy + (map->ray.planey * \
			(map->ray.movespeed))), map))
		{
			map->ray.posx += map->ray.planex * 0.1;
			map->ray.posy += map->ray.planey * 0.1;
		}
	}
}

void		ft_move_rotate_xinv(t_map *map, double oldirx, double oldplanex)
{
	if (map->img.rotate_right == 1)
	{
		oldirx = map->ray.dirx;
		oldplanex = map->ray.planex;
		map->ray.dirx = map->ray.dirx * cos(map->ray.rotspeed / 2) -
			map->ray.diry * sin(map->ray.rotspeed / 2);
		map->ray.diry = oldirx * sin(map->ray.rotspeed / 2) + map->
			ray.diry * cos(map->ray.rotspeed / 2);
		map->ray.planex = map->ray.planex * cos(map->ray.rotspeed / 2) -
			map->ray.planey * sin(map->ray.rotspeed / 2);
		map->ray.planey = oldplanex * sin(map->ray.rotspeed / 2) +
			map->ray.planey * cos(map->ray.rotspeed / 2);
	}
}

/*
** move rotate x c'est surtout par rapport à la caméra
** même principe que les fonctions précédentes
** on va d'abord enregistrer l'ancienne direction
** et on va calculer le vecteur dans la nouvelle (dirx)
** puis on enregistre le plan ancien
** pour calculer le nouvel
*/

void		ft_move_rotate_x(t_map *map)
{
	double oldirx;
	double oldplanex;

	oldplanex = map->ray.planex;
	oldirx = map->ray.dirx;
	if (map->img.rotate_left == 1)
	{
		map->ray.dirx = map->ray.dirx * cos(-map->ray.rotspeed / 2) -
			map->ray.diry * sin(-map->ray.rotspeed / 2);
		map->ray.diry = oldirx * sin(-map->ray.rotspeed / 2) +
			map->ray.diry * cos(-map->ray.rotspeed / 2);
		map->ray.planex = map->ray.planex * cos(-map->ray.rotspeed / 2)
			- map->ray.planey * sin(-map->ray.rotspeed / 2);
		map->ray.planey = oldplanex * sin(-map->ray.rotspeed / 2) +
			map->ray.planey * cos(-map->ray.rotspeed / 2);
	}
	ft_move_rotate_xinv(map, oldirx, oldplanex);
}
