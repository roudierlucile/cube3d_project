# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lroudier <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/13 10:17:33 by lroudier          #+#    #+#              #
#    Updated: 2020/12/14 09:23:42 by lroudier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cube3D

LBPATH = ./libft/
LBNAME = $(LBPATH)libft.a

MLXPATH = ./minilibx/
MLXNAME = $(MLXPATH)libmlx.a

SRCPATH = src/
SRCS = $(SRCPATH)ft_utils_map.c $(SRCPATH)ft_bmp.c $(SRCPATH)ft_utils.c $(SRCPATH)ft_map_treat.c $(SRCPATH)ft_parse_color.c $(SRCPATH)ft_init.c $(SRCPATH)ft_treat_texture.c $(SRCPATH)ft_move_boy.c $(SRCPATH)mlx_command.c $(SRCPATH)ft_raycasting.c $(SRCPATH)ft_sprite.c $(SRCPATH)ft_parser_map.c $(SRCPATH)ft_drawmap.c main.c \

INCLUDE = cube3D.h

OBJS_NAME = ${SRCS:.c=.o}

OBJS_PATH = ./objs/

OBJS = $(addprefix $(OBJS_PATH), $(OBJS_NAME))

CC	 =	clang -Wall -Werror -Wextra -g 

# GRAPHICAL LFGLAGS (for linux):
LDFLAGS	 = -lX11 -lXext -lm

RM	 = rm -rf

all: $(NAME)

$(NAME): $(OBJS) $(LIBFTM) $(MLXM)
	@make -C $(LBPATH)
	@make -C $(MLXPATH)
	$(CC) $(OBJS) $(LBNAME) $(MLXNAME) -o $(NAME) $(LDFLAGS)
	@echo "  ______     __  __     ______     ______     _____ ";
	@echo " /\  ___\   /\ \/\ \   /\  == \   /\  ___\   /\  __-. ";
	@echo " \ \ \____  \ \ \_\ \  \ \  __<   \ \  __\   \ \ \/\ \ ";
	@echo "  \ \_____\  \ \_____\  \ \_____\  \ \_____\  \ \____- ";
	@echo "   \/_____/   \/_____/   \/_____/   \/_____/   \/____/ ";
	@echo " ";
	@echo "\033[0;32m IT 'S COMPILED \033[0m| \033[0;31m SOMEBODY ONCE TOLD ME \033[0m";
	@echo " ";
	@echo "  _____  _____  _____    _____  _____  _____  _____  _____  _____  ____ ";
	@echo " |   __||   __||_   _|  |   __||  |  || __  ||   __||  |  ||   __||    \ ";
	@echo " |  |  ||   __|  | |    |__   ||     ||    -||   __||    -||   __||  |  | ";
	@echo " |_____||_____|  |_|    |_____||__|__||__|__||_____||__|__||_____||____/ ";

$(LIBFTM):
	make -C $(LBPATH) -f Makefile

$(MLXM):
	make -C $(MLXPATH) -f Makefile

$(OBJS_PATH)%.o : %.c
	@mkdir $(OBJS_PATH) 2> /dev/null || true
	@mkdir $(OBJS_PATH)$(SRCPATH) 2> /dev/null || true
	@$(CC) -I $(INCLUDE) -c $< -o $@

libft:	$(LIBFTM)

mlx:	$(MLXM)

clean:
	$(RM) $(OBJS)
	make -C $(LBPATH) -f Makefile clean
	make -C $(MLXPATH) -f Makefile clean
	@rmdir $(OBJS_PATH) 2> /dev/null || true
	@rmdir $(OBJS_PATH)$(SRCPATH) 2> /dev/null || true

fclean: clean
	$(RM) $(NAME) $(MLXM)
	make -C $(LBPATH) -f Makefile fclean

re: fclean $(NAME)

.PHONY: all, clean, fclean, re, libft, mlx
