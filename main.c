/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lroudier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 09:45:27 by lroudier          #+#    #+#             */
/*   Updated: 2021/04/14 16:20:25 by lroudier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./src/ft_cube3d.h"

int		ft_run(t_map *map)
{
	map->ray.x = -1;
	while (++map->ray.x < map->widthwin)
	{
		ft_init_ray(map);
		ft_init_sidedist(map);
		ft_perform_dda(map);
		ft_dist_cam_dir(map);
		ft_draw_col(map);
		map->sprite.zbuffer[map->ray.x] = map->ray.perpwalldist;
	}
	ft_start_sprite(map);
	if (map->save == 1)
		ft_save_bmp(map);
	else
	{
		mlx_put_image_to_window(map->mlx, map->win, map->img.img, 0, 0);
		ft_move_y(map);
		ft_move_x(map);
		ft_move_rotate_x(map);
	}
	return (1);
}

void	map_size(t_map *map)
{
	mlx_get_screen_size(map->mlx, &map->widthscreen, &map->heightscreen);
	if (map->widthwin > map->widthscreen)
		map->widthwin = map->widthscreen;
	if (map->heightwin > map->heightscreen)
		map->heightwin = map->heightscreen;
}

void	init_mlx(t_map *map)
{
	map_size(map);
	map->img.img = mlx_new_image(map->mlx, map->widthwin, map->heightwin);
	map->img.addr = (int *)mlx_get_data_addr(map->img.img, \
			&map->img.bits_per_pixel, &map->img.line_length, &map->img.endian);
	if (map->save == 1)
		ft_run(map);
	else
	{
		map->win = mlx_new_window(map->mlx, map->widthwin, \
				map->heightwin, "CUBE 3D HAIL DARTH VADER");
		mlx_hook(map->win, 33, 1L << 17, close_win, map);
		mlx_hook(map->win, 2, 1L << 0, move_command, map);
		mlx_loop_hook(map->mlx, ft_run, map);
		mlx_hook(map->win, 3, 1L << 1, move_command_release, map);
		mlx_loop(map->mlx);
	}
}

void	check_map_arg(char *argv[], t_map *map)
{
	if (!parse_args(argv, map) || map->player != 1)
	{
		if (map->player < 1 || map->player > 1)
			ft_putstr_fd("Error\n YOU NEED PLAYER DUMB AND U ARE ALONE\n", 1);
		ft_putstr_fd("Error\n Your map is not correct\n", 1);
		ft_free_datas(map, 1);
	}
	if (map->countmap != 3 || map->nbrtext != 11)
	{
		ft_putstr_fd("Error\n YOU NEED COLORS OR SIZE OR TEXTURE\n", 1);
		ft_free_datas(map, 1);
	}
}

int		main(int argc, char *argv[])
{
	t_map *map;

	if (argc > 1 && argc <= 3)
	{
		if (!(map = malloc(sizeof(t_map))))
			ft_putstr_fd("Error\n Allocation  map\n", 1);
		init_map(map);
		if (argc == 3)
		{
			if (check_bmp(argv[2]) == 1)
				map->save = 1;
			else
			{
				ft_putstr_fd("Error\n Arguments for save is not okay\n", 1);
				ft_free_datas(map, 1);
			}
		}
		check_map_arg(argv, map);
		ft_nbr_sprite_pos(map);
		init_pos(map);
		init_mlx(map);
	}
	else
		ft_putstr_fd("Error\n Problems with arguments\n", 1);
	return (0);
}
